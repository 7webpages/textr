import string
import random
import HTMLParser
from BeautifulSoup import BeautifulSoup


def smart_truncate(content, length=100, suffix='...'):
    """This is really awesome. --> This is really..."""
    if len(content) <= length:
        return content
    else:
        return ' '.join(content[:length + 1].split(' ')[0:-1]) + suffix


def randomword(length):
    s = string.lowercase + string.ascii_uppercase
    return ''.join(random.choice(s) for i in range(length))


def make_title(html):
    """Generate smart title from html code"""

    def preprocess(s):
        """Replace html entities with the corresponding utf-8 characters"""
        p = HTMLParser.HTMLParser()
        s = p.unescape(s)
        s = s.strip()
        return s

    if not html:
        return ''

    soup = BeautifulSoup(html)
    text_parts = soup.findAll(text=True)
    text_parts = [preprocess(t) for t in text_parts]
    text = ' '.join(text_parts)

    text = smart_truncate(text, length=150)

    return text


