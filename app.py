# -*- coding: utf-8 -*-
import logging

from flask import Flask
from flask import render_template
from flask import request, redirect

import leveldb
import scrubber

from logging.handlers import RotatingFileHandler

from utils import randomword, make_title


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        content = request.form['content']

        content = content[0:1000000]
        content = scrubber.Scrubber().scrub(content)

        key = randomword(8)
        url = "/%s/" % key

        db = leveldb.LevelDB('./db')
        content = content.encode('utf-8')
        db.Put(key, content)

        return redirect(url)

    return render_template('index.html')


@app.route('/<key>/')
def show_text(key):
    try:
        db = leveldb.LevelDB('./db')
        content = db.Get(key)
        content = content.decode('utf-8')
    except KeyError:
        content = None

    title = make_title(content)

    return render_template('show_text.html',
        content=content,
        title=title,
    )


if __name__ == '__main__':

    handler = RotatingFileHandler('logs/app.log', maxBytes=10000,
        backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)

    app.debug = True
    app.run(host='0.0.0.0')
